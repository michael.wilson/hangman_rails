require 'rails_helper'

RSpec.describe Guess, type: :model do

  let (:game) { Game.create!(word: "word", lives: 8) }
  let (:value) { "a" }

  subject (:guess) { game.guesses.new(value: value) }

  context 'does not validate symbols' do
    let (:value) { "#" }
    it 'invalid' do
      expect(guess).to_not be_valid
    end
  end

  context 'only validates one letter' do
    let (:value) { "dee" }
    it 'too many letters' do
      expect(guess).to_not be_valid
    end
  end

  context 'cant guess the same letter twice' do
    let (:second_guess) { game.guesses.new(value: value) }
    
    it 'invalid second guess' do
      guess.save!
      expect(second_guess).to_not be_valid
    end
  end

  context 'downcases the guess' do
    let (:value) { "F" }
    it 'invalid second guess' do
      guess.save!
      expect(guess.value).to eq("f")
    end
  end

  context 'cannot be an empty string' do
    let (:value) { "" }
    it 'invalid guess' do
      expect(guess).to_not be_valid
    end
  end
end
