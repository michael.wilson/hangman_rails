require 'rails_helper'

RSpec.describe Game, type: :model do

  let (:word) { "powershop" }
  let (:lives) { 8 }
  let (:game) { Game.new(word: word, lives: lives) }
  
  context 'has attributes' do
    it 'has a word attribute' do
      expect(game.word).to eq("powershop")
    end

    it 'has a lives attribute' do
      expect(game.lives).to eq(8)
    end
  end

  context 'validates presence of attributes' do
    let (:lives) { nil }
    it 'validates presence of lives' do
      expect(game).to_not be_valid
    end

    let (:word) { nil }
    it 'validates presence of word' do
      expect(game).to_not be_valid
    end
  end

  context 'changes the word into a uniq sorted array' do
    it 'returns the correct array' do
      expect(game.word_array).to eq(["e", "h", "o", "p", "r", "s", "w"])
    end
  end

  context 'game is over' do
    let (:lives) { 0 }
    let (:word) { "word" }
    it '#win?' do
      game.save
      expect(game.guesses).to receive(:pluck).and_return(["o","d","r","w"])
      expect(game.win?).to eq(true)
    end

    it '#lost' do
      game.save
      expect(game.lost?).to eq(true)
    end
  end
end
