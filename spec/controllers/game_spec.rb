require 'rails_helper'

RSpec.describe GameController, type: :controller do

  let (:valid_attributes) do
    {
      word: "powershop",
      lives: 7
    }
  end

    let (:invalid_attributes) do
    {
      word: "",
      lives: 7
    }
  end

  it 'redirects a game made' do
    post :create, params: {game: valid_attributes } 
    expect(response).to redirect_to(Game.last)
  end

  it 'redirects a new' do
    post :create, params: {game: invalid_attributes } 
    expect(response).to be_success
  end

end
