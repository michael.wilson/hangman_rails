module GameHelper

  def error_value(value)
    value.strip.empty? ? 'invalid " ",' : value
  end

end
