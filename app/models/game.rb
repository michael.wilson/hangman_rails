class Game < ApplicationRecord
  has_many :guesses
  validates :lives, :word, presence: true

  def mask_word
    word.chars.map do |f|
      guess_values.include?(f.downcase) ? f : "_"
    end.join(" ")
  end

  def calculate_lives
    lives - incorrect_guesses
  end

  def incorrect_guesses
    guess_values.reject { |letter| word.chars.include?(letter) }.length
  end

  def word_array
    word.downcase.chars.uniq.sort
  end

  def guess_values
    guesses.pluck(:value)
  end

  def in_progress
    !lost? && !win?
  end

  def lost?
    calculate_lives == 0
  end

  def win?
    (word_array & guess_values).sort == word_array
  end
end
