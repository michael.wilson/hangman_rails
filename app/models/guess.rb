class Guess < ApplicationRecord
  belongs_to :game
  validates :value, format: { with: /\A[a-zA-Z]+\z/,
  message: "only allows letters" }
  validates :value, length: { is: 1 }, allow_blank: false
  validates :value, uniqueness: { scope: :game }
  before_validation :downcase_guess

  def downcase_guess
    self.value = self.value.downcase
  end
end
