class GameController < ApplicationController
  before_action :errors, only: [:show]
  def new
    @game = Game.new
  end

  def show
    @game = Game.find(params[:id])
    @guess = Guess.new
  end

  def create
    @game = Game.new(game_params)
    if @game.save
      redirect_to @game
    else
      render 'new'
    end
  end

  private

  def errors
    @errors = { error: params[:error], value: params[:value] }
  end

  def game_params
    params.require(:game).permit(:word, :lives)
  end
end
