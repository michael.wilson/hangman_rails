class GuessesController < ApplicationController
  
  before_action :find_game
  attr_reader :game, :guess

  def create
    @guess = game.guesses.build(guesses_params)
    if @guess.save
      redirect_to game
    else
      redirect_to error_params
    end
  end

  private

  def find_game
    @game = Game.find(params[:game_id])
  end

  def guesses_params
    params.require(:guess).permit(:value)
  end

  def error_params
    {
      controller: "game",
      action: "show",
      id: game.id,
      error: guess.errors.messages[:value],
      value: guess.value
    }
  end
end
