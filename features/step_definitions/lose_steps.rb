Given(/^I am a player with 1 life left$/) do
  visit new_game_path
  fill_in "game[word]", with: 'word'
  fill_in "game[lives]", with: 1
  click_button('Play?')
end

And(/^I guess the wrong letter$/) do
  fill_in "guess[value]", with: "p"
end

When(/^I press the guess button$/) do
  click_button('Guess')
end

Then(/^I should lose$/) do
  expect(page).to have_content("you lost")
end
