Given(/^I am a player with 1 letter left to guess$/) do
  visit new_game_path
  fill_in "game[word]", with: 'yes'
  fill_in "game[lives]", with: 5
  click_button('Play?')
  fill_in "guess[value]", with: "y"
  click_button('Guess')
  fill_in "guess[value]", with: "e"
  click_button('Guess')
end

And(/^I guess the right letter$/) do
  fill_in "guess[value]", with: "s"
end

When(/^I press the guess button again$/) do
  click_button('Guess')
end

Then(/^I should win$/) do
  expect(page).to have_content("WON")
end
