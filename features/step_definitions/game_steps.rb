Given(/^on the new game page$/) do
  visit new_game_path
end

And(/^I choose a word and how many lives I have$/) do
  fill_in "game[word]", with: 'flux'
  fill_in "game[lives]", with: 8
end

When(/^I start the game$/) do
  click_button('Play?')
end

Then(/^I see the masked word$/) do
  expect(page).to have_content("_ _ _ _")
end

Then(/^I see how many lives I have$/) do
  expect(page).to have_content("8")
end
