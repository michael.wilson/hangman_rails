Feature: Game Feature
As a player
I want to play hangman

Scenario: Player can start a new game
Given on the new game page
And I choose a word and how many lives I have
When I start the game
Then I see the masked word
Then I see how many lives I have

Scenario: Player wins the game
Given I am a player with 1 letter left to guess
And I guess the right letter
When I press the guess button again
Then I should win

Scenario: Player loses the game
Given I am a player with 1 life left
And I guess the wrong letter
When I press the guess button
Then I should lose
