class CreateGuesses < ActiveRecord::Migration[5.1]
  def change
    create_table :guesses do |t|
      t.belongs_to :game
      t.string :value
      t.timestamps
    end
  end
end
